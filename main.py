import os 
import time
import os.path
import shutil
from substitute import substitute




#get current working directory, cwd
base_path = "S:/Kurser/"

def main():
    
    project_name = input("Input the project title: ")
    project_description = input("Give the project a description: ")
    author = input("Who's the author?: ")
    email = input("Input the author email: ")
    language = input("Input what language the document will be written in: ")
    course_name = input("Which course is the project for?: ")
    course_code = input("Input the Course code: ")
    semester = input("Which semester is the project written in? e.g. HT21: ")
    folder_name = input("Input the name you want the project folder to have: ")

    #assumes the maps for the courses lies in S:/Kurser/
    path = os.path.join(base_path, course_code)
    
    #if there isnt already a map for that course, create one
    if not os.path.isdir(path):
        os.mkdir(path)
    
    path = os.path.join(path, folder_name) 
    #os.mkdir(path) #create the directory for the new latex projext
    shutil.copytree("template/", path)
    os.chdir(path) #change directory to the new latex project
    print(os.getcwd())

    frontpage_list = [project_name, project_description, author, email]
    preamble_list = [project_name, author, course_name, course_code, semester]
    main_list = [language]
    
    substitute(frontpage_list, "content/frontpage.tex")
    substitute(preamble_list, "setup/preamble.tex")
    substitute(main_list, "main.tex")

if __name__ == "__main__":
    main()
