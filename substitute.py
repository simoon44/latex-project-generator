import re

def substitute(lst, filename):

    for substitution in lst:
        
        patterns_frontpage = ["<TITLE>", "<DESCRIPTION>", "<AUTHOR>", "<EMAIL>"] 
        patterns_main = ["<LANGUAGE>"]
        patterns_preamble = ["<TITLE>", "<AUTHOR>", "<COURSENAME>", "<COURSECODE>", "<SEMESTER>"]
        
        if filename == "content/frontpage.tex":
            patterns = patterns_frontpage
        elif filename == "setup/preamble.tex":
            patterns = patterns_preamble
        else:
            patterns = patterns_main

        
        to_loop = zip(patterns, lst)

        with open(filename, "r") as rfile:
            data = rfile.read()

        for entry in to_loop:
            data = re.sub(entry[0], entry[1], data)

        with open(filename, "w") as wfile:
            wfile.write(data)
        
