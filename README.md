# Latex-project generator

Intended to use a python script to create a custom Latex-project from already existing templates. Customizes such as language, title, and so on... But keeps the overall project topics and files and makes it easy to start editing.